#ifndef __SYS_H
#define __SYS_H

#include "main.h"
#include "stm32l0xx_hal.h"


typedef  uint32_t u32;
typedef  uint16_t u16;
typedef  uint8_t u8;

#define TRUE  1
#define FALSE 0

extern SPI_HandleTypeDef hspi2;


extern void sys_init(void);
extern void main_loop(void);

#endif
