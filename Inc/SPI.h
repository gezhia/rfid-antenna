#ifndef _SPI_H
#define _SPI_H

extern uint8_t SPI_SetReg(uint8_t ucRegAddr, uint8_t ucRegVal);
extern uint8_t SPI_GetReg(uint8_t ucRegAddr);
extern void SPI_Read_Sequence(unsigned char sequence_length,unsigned char ucRegAddr,unsigned char *reg_value);
extern void SPI_Write_Sequence(unsigned char sequence_length,unsigned char ucRegAddr,unsigned char *reg_value);

//extern void SPI_dome(void);


#endif
