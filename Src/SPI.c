#include "sys.h"
#include "SPI.h"

#define CS_LOW HAL_GPIO_WritePin(RFID_CS_GPIO_Port,RFID_CS_Pin,GPIO_PIN_RESET);
#define CS_HIGH HAL_GPIO_WritePin(RFID_CS_GPIO_Port,RFID_CS_Pin,GPIO_PIN_SET);

uint8_t spi_RxData[2];

//void SPI_dome(void)
//{
////    uint8_t tx[2] = {ucRegAddr,0},rx[2];
////    tx[0] = ucRegAddr;
////    tx[1] = 0;
////    rx[0] = 0;
////    rx[1] = 0;
////    HAL_SPI_TransmitReceive(&hspi2,tx,rx,2,0x1000);
////    HAL_SPI_Transmit(&hspi2,tx,1,0x1000);
////    HAL_SPI_Receive(&hspi2,rx,1,0x1000);
//    
//    
//        

//}



/*********************************************************************************************************
** Function name:       spi_SetReg
** Descriptions:        SPIд����оƬ�Ĵ�������
** input parameters:    ucRegAddr���Ĵ�����ַ
**                      ucRegVal��Ҫд���ֵ
** output parameters:   ��
** Returned value:      TRUE
*********************************************************************************************************/
uint8_t SPI_SetReg(uint8_t ucRegAddr, uint8_t ucRegVal)
{
    uint8_t tx[2],RxData[2];
    CS_LOW;
    tx[0] = ucRegAddr << 1;
    tx[1] = ucRegVal;
    //HAL_SPI_Transmit(&hspi2,tx,2,5000);
    HAL_SPI_TransmitReceive(&hspi2,tx,RxData,2,5000);
    HAL_Delay(1);
    CS_HIGH;
	return TRUE;
}


/*********************************************************************************************************
** Function name:       spi_GetReg
** Descriptions:        SPI������оƬ�Ĵ�������
** input parameters:    ucRegAddr���Ĵ�����ַ; uint8_t *p ���ݴ�ŵ�ַ
** output parameters:   ��
** Returned value:      Ŀ��Ĵ�����ֵ
*********************************************************************************************************/

uint8_t SPI_GetReg(uint8_t ucRegAddr)
{
    uint8_t TxData[2],RxData[2];
    CS_LOW;
    TxData[0] = 0x80|(ucRegAddr<<1);
    TxData[1] = 0;
    HAL_SPI_TransmitReceive(&hspi2,TxData,RxData,2,5000);
  
    CS_HIGH;
	return RxData[1];
}


//uint8_t SPI_GetReg(uint8_t ucRegAddr)
//{
//    uint8_t TxData[2];
//    CS_LOW;
//    TxData[0] = 0x80|(ucRegAddr<<1);
//    TxData[1] = 0;

//    switch(HAL_SPI_TransmitReceive(&hspi2,TxData,spi_RxData,2,5000))
//    {
//        case HAL_OK:
//            spi_RxData[0] = 0;
//        break;
//        case HAL_TIMEOUT:
//            spi_RxData[0] = 0;
//        break;
//        case HAL_ERROR:
//            spi_RxData[0] = 0;
//        case HAL_BUSY:
//            spi_RxData[0] = 0;
//        break;
//    }
//            
//    
//    CS_HIGH;
//	return spi_RxData[1];
//}



/*********************************************************************************************************
** Function name:       SPIRead_Sequence
** Descriptions:        SPI?FIFO?????
** input parameters:    sequence_length ???? ucRegAddr:?????  *reg_value ????
** output parameters:   ?
** Returned value:      ?
*********************************************************************************************************/
void SPI_Read_Sequence(unsigned char sequence_length,unsigned char ucRegAddr,unsigned char *reg_value)    
{
    
    uint8_t i;
    if (sequence_length==0)
    return;
    //CD_EN_NESS;
    for(i=0;i<sequence_length;i++) {
        *(reg_value+i) = SPI_GetReg(ucRegAddr);
    }
    //CD_EN_NESS;

    return;

}

/*********************************************************************************************************
** Function name:       SPIWrite_Sequence
** Descriptions:        SPI?FIFO??
** input parameters:    sequence_length ???? 
**                      ucRegAddr:?????  
**                      *reg_value ????
** output parameters:   ?
** Returned value:      ?
*********************************************************************************************************/
void SPI_Write_Sequence(unsigned char sequence_length,unsigned char ucRegAddr,unsigned char *reg_value)
{
    
    uint8_t i;
    if(sequence_length==0)
        return;
    //CD_EN_NESS;
    for(i=0;i<sequence_length;i++) {
        SPI_SetReg(ucRegAddr, *(reg_value+i));
    }
    //CD_DIS_NESS;
    return ;    
}


/************************* the code end **********************/
