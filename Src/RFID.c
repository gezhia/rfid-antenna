

#include "sys.h"
#include "RFID.h"
#include "fm175xx.h"
#include "rfid_type_a.h"
#include "rfid_type_b.h"


#define RFID_EN_TIME 10

uint8_t RFID_code[8];
uint32_t RFID_in,RFID_en_time;


void RFID_Initial(void)
{
    
    
    RFID_in = 0;
    //HAL_GPIO_WritePin(PWR_EN_GPIO_Port,PWR_EN_Pin,GPIO_PIN_SET);
    
    pcd_Init();
    MCU_TO_PCD_TEST();
}


/*********************************************************************************************************
** Function name:       TyteA_Test()
** Descriptions:        ISO14443A????
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*********************************************************************************************************/
void TyteA_Test ()
{
    uint8_t statues = TRUE;
    uint8_t i=0;
    uint8_t picc_atqa[2];                                               /* ????????????     */
    static uint8_t picc_uid[15];                                        /* ????UID??              */
    uint8_t picc_sak[3];                                                /* ????????             */
    
    FM175X_SoftReset( );                                                /* FM175xx????              */
    HAL_Delay(1);
    Set_Rf( 3 );                                                        /* ?????                   */
    Pcd_ConfigISOType( 0 );                                             /* ISO14443??????         */
    while(i <2 ) 
    {
        statues = TypeA_CardActive( picc_atqa,picc_uid,picc_sak );      /* ????                     */
        if ( statues == TRUE ) 
        {
            
            TypeA_Halt(0);                                              /* ????                     */
            for(i = 0;i < 4;i ++)
            {
                RFID_code[i] = picc_uid[i];
                RFID_code[i+4] = 0xFF;
            }
            
            RFID_in = 1;      
        }
        else 
        {
            i++;
        }                    
    }
    
}


/*********************************************************************************************************
** Function name:       TyteB_Test()
** Descriptions:        ISO14443B????
** input parameters:    ?
** output parameters:   ?
** Returned value:      ?
*********************************************************************************************************/
void TyteB_Test ()
{
    uint8_t statues;
    uint32_t rec_len;
    uint8_t pupi[4];
    uint8_t buff[12];
    FM175X_SoftReset( );                                                /* FM175xx????              */
    Pcd_ConfigISOType( 1 );                                             /* ISO14443??????         */
    Set_Rf( 3 );                                                        /* ?????                   */ 
    statues = TypeB_WUP(&rec_len,buff,pupi);                            /* ??                         */
    if ( statues == TRUE ) {
        statues = TypeB_Select(pupi,&rec_len,buff); 
    }
    if ( statues == TRUE ) {
        

        statues = TypeB_GetUID(&rec_len,&buff[0]);
        if(statues == TRUE) {

            HAL_Delay(1);
       }
  
    }
    Set_Rf( 0 );                                                        /* ????                     */    
}

void RFID_read(void)
{
    //if(0 == RFID_in)
    {
        //HAL_Delay(10);
        //if(0 == RFID_en_time)
        {
            TyteB_Test();
            TyteA_Test();
            RFID_en_time = RFID_EN_TIME;
        }
    }
}

/************************* the code end **********************/
